from machine import Pin
from onewire import OneWire
from ds18x20 import DS18X20
import time

wire = OneWire(Pin(20))
ds_wire = DS18X20(wire)

devices = ds_wire.scan()

print('Found DS devices:')
for dev in devices:
    print(f'    Serial code 0x{dev.hex()}')

while True:
    ds_wire.convert_temp()
    for dev in devices:
        temp = ds_wire.read_temp(dev)
        print(f'Temperature: {temp:.4f} ℃')
