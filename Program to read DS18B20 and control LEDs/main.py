from machine import Pin
from onewire import OneWire
from ds18x20 import DS18X20
import time

led_green = Pin('LED', Pin.OUT)
led_red = Pin(16, Pin.OUT)

wire = OneWire(Pin(20))
ds_wire = DS18X20(wire)

devices = ds_wire.scan()

print('Found DS devices:')
for dev in devices:
    print(f'    Serial code 0x{dev.hex()}')

while True:
    ds_wire.convert_temp()
    for dev in devices:
        temp = ds_wire.read_temp(dev)
        print(f'Temperature: {temp:.4f} ℃')
        if 25.0 <= temp <= 27.0:
            led_green.on()
            led_red.off()
        else:
            led_green.off()
            if temp < 25.0:
                led_red.on()
            else:
                led_red.toggle()
    time.sleep_ms(483)
    time.sleep_us(600)
