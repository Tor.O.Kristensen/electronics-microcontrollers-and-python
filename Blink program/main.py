from machine import Pin
import time

led_red = Pin(16, Pin.OUT)

while True:
    led_red.on()
    time.sleep_ms(100)
    led_red.off()
    time.sleep_ms(400)
